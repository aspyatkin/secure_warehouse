require 'spec_helper'

describe User do

  before do
    @user = User.new(login: "aspyatkin", first_name: "Alexander", second_name: "Pyatkin",
      password: "l0r3m1psum", password_confirmation: "l0r3m1psum")
  end
  
  subject { @user }

  it { should respond_to(:login) }
  it { should respond_to(:first_name) }
  it { should respond_to(:second_name) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  
  it { should be_valid }
  it { should respond_to(:authenticate) }
  
  describe "when login is not present" do
    before { @user.login = " " }
    it { should_not be_valid }
  end

  describe "when first_name is not present" do
    before { @user.first_name = " " }
    it { should_not be_valid }
  end
  
  describe "when second_name is not present" do
    before { @user.second_name = " " }
    it { should_not be_valid }
  end    
end
