class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.string :title
      t.string :description
      t.integer :category
      t.integer :user_id
      t.boolean :shared

      t.timestamps
    end
	add_index :units, [:user_id]
  end
end
