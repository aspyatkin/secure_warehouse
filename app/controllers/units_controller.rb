class UnitsController < ApplicationController
  before_action :signed_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy  

  def initialize
    @categories = ['Weapon', 'Survival gear', 'Clothes', 'Surveillance', 'Medicament', 'Computing equipment', 'Spare parts', 'Other']
    
    super
  end
  
  def categories_select
    select = []
	(1..@categories.length).to_a.each { |i| select.push([@categories[i-1], i]) }  
    select
  end
  
  def new
    @unit = Unit.new
  end

  def create
    @unit = current_user.units.build(unit_params)
    if @unit.save
      flash[:success] = "Unit is stored!"
      redirect_to units_path
    else
      render 'new'
    end
  end

  def destroy
    @unit.destroy
	redirect_to units_path
  end
  
  def index
    @query = params[:q]
	@search_in = params[:filter_content]
	@commit = params[:commit]
	@only_categories = params[:filter_category]
	if @only_categories == nil
		@only_categories = []
	end
	if @query != '' and @commit == 'find'
	  part_by_title = "title like '%#{@query}%'"
	  part_by_description = "description like '%#{@query}%'"
	  whole_query = ''
	  
	  category_part = ''
	  only_categories_strings = []
	  @only_categories.each do |i|
	    category_number = i.to_i
	    if category_number >= 1 and category_number <= 8
	      only_categories_strings.push "category=#{i}"
		end
	  end
	  category_part = only_categories_strings.join(' or ')
	  if category_part != ''
	    category_part = '(' + category_part + ') and '
	  end
	  
	  if @search_in == 'in_title'
	    whole_query = "#{category_part}#{part_by_title}"
	  elsif @search_in == 'in_description'
	    whole_query = "#{category_part}#{part_by_description}"
	  elsif @search_in == 'in_both'
	    whole_query = "#{category_part}#{part_by_title} or #{part_by_description}"
	  end
	  
	  if whole_query != ''
	    @units = current_user.units.where(whole_query).paginate(page: params[:page])
	  else
	    @units = current_user.units.paginate(page: params[:page])
	  end
	else
	  @units = current_user.units.paginate(page: params[:page])
    end
  end
  
  private
    def unit_params
      params.require(:unit).permit(:title, :description, :category, :shared)
    end

    def correct_user
      @unit = current_user.units.find_by(id: params[:id])
      redirect_to root_url if @unit.nil?
    end  
end
