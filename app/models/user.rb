class User < ActiveRecord::Base
  has_many :units, dependent: :destroy
  before_save { self.login = login.downcase }
  before_create :create_remember_token
  
  validates :login, presence: true, length: { maximum: 30 }, uniqueness: { case_sensitive: false }
  validates :first_name, presence: true, length: { maximum: 50 }
  validates :second_name, presence: true, length: { maximum: 50 }
  
  has_secure_password
  validates :password, length: { minimum: 6 }
  
  def new_remember_token
    #SecureRandom.urlsafe_base64
	Digest::SHA2.hexdigest(self.login + Time.now.to_s + SecureRandom.random_number(100).to_s)
  end
  
  def User.encrypt(token)
    Digest::SHA2.hexdigest(token.to_s)
  end
  
  private
    def create_remember_token
	  self.remember_token = User.encrypt(new_remember_token)
	end
end
